﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassPasswordChange
{
	class TextReplacement
	{
		public string SearchText { get; set; }
		public string ReplacementText { get; set; }

		public TextReplacement(string searchText, string replacementText)
		{
			this.SearchText = searchText;
			this.ReplacementText = replacementText;
		}
	}
}