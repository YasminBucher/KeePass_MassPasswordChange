﻿using System.Collections.Generic;
using KeePassLib;
using KeePassLib.Cryptography.PasswordGenerator;

namespace MassPasswordChange
{
	public class MassPasswordChangeSettings
	{
		public PwProfile Profile { get; private set; }
		public List<PwEntry> Entries { get; private set; }
		public bool WriteChangesToFile { get; private set; }
		public string OutputFilePath { get; private set; }
		public string FileEntryFormat { get; private set; }

		public MassPasswordChangeSettings(
			PwProfile profile,
			List<PwEntry> entries,
			bool writeChangesToFile = false,
			string outputFilePath = "",
			string fileEntryFormat = "")
		{
			this.Profile = profile;
			this.Entries = entries;
			this.WriteChangesToFile = writeChangesToFile;
			this.OutputFilePath = outputFilePath;
			this.FileEntryFormat = fileEntryFormat;
		}
	}
}