﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassPasswordChange
{
	class TextReplacementManager
	{
		private List<TextReplacement> Replacements;

		public TextReplacementManager()
		{
			this.Replacements = new List<TextReplacement>();
		}

		public void Set(string search, string replacement)
		{
			TextReplacement repl = this.GetReplacement(search);

			if(repl == null)
			{
				this.Replacements.Add(new TextReplacement(search, replacement));
			}
			else
			{
				repl.ReplacementText = replacement;
			}
		}

		private TextReplacement GetReplacement(string search)
		{
			return this.Replacements.FirstOrDefault(r => r.SearchText == search);
		}

		public string PerformReplacements(string text)
		{
			foreach(TextReplacement repl in this.Replacements)
			{
				text = text.Replace(repl.SearchText, repl.ReplacementText);
			}

			return text;
		}
	}
}