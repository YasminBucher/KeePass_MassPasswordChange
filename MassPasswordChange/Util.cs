﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using KeePass.Util;
using KeePassLib;
using KeePassLib.Cryptography.PasswordGenerator;

namespace MassPasswordChange
{
	internal class Util
	{
		private static Version appVersion;

		static Util()
		{
			appVersion = Assembly.GetExecutingAssembly().GetName().Version;
		}

		private Util()
		{

		}

		public static Version GetAppVersion()
		{
			return appVersion;
		}

		public static List<PwProfile> GetProfiles()
		{
			List<PwProfile> profiles = PwGeneratorUtil.GetAllProfiles(true);
			return profiles;
		}

		public static bool IsPathValid(string filePath)
		{
			bool isValid = false;

			try
			{
				FileInfo fi = new FileInfo(filePath);

				if(!fi.Exists)
				{
					using(FileStream fs = fi.Create())
					{

					}

					fi.Refresh();

					if(fi.Exists)
					{
						fi.Delete();
					}
				}

				isValid = true;
			}
			catch(Exception)
			{
				isValid = false;
			}

			return isValid;
		}

		public static void ShowInformationBox(string text, string caption = "Info")
		{
			MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public static void ShowWarningBox(string text, string caption = "Warning")
		{
			MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public static void ShowErrorBox(string text, string caption = "Error")
		{
			MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}