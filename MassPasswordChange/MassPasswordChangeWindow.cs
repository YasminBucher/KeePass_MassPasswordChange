﻿using KeePassLib.Cryptography.PasswordGenerator;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using KeePassLib;
using KeePass.Plugins;
using System.Collections;

namespace MassPasswordChange
{
	public partial class MassPasswordChangeWindow : Form
	{
		private const string INDICATOR_GROUP = "[G]";
		private const string INDICATOR_ENTRY = "[E]";

		private bool blockEventHandling = false;

		public List<PwEntry> SelectedEntries { get; private set; }

		public MassPasswordChangeWindow(PwGroup rootGroup)
		{
			this.InitializeComponent();

			this.ToggleFileOutput();
			this.SelectedEntries = null;

			Version version = Util.GetAppVersion();
			this.Text = string.Format("{0} - v{1}.{2}.{3}", this.Text, version.Major, version.Minor, version.Revision);

			this.InitProfileSelection();
			this.InitEntrySelection(rootGroup);
			this.initPlaceholderSelection();

			this.txtOutputFile.Text = Settings.OutputFilePath;
			this.txtFileEntryFormat.Text = Settings.FileEntryFormat;
			this.chkOutputChangesToFile.Checked = Settings.WriteChangesToFile;
		}

		private void ToggleFileOutput()
		{
			this.lblOutputFile.Enabled
				= this.txtOutputFile.Enabled
				= this.btnSelectOutputFile.Enabled
				= this.lblFileEntryFormat.Enabled
				= this.txtFileEntryFormat.Enabled
				= this.chkOutputChangesToFile.Checked;
		}

		private void InitProfileSelection()
		{
			List<PwProfile> profiles = Util.GetProfiles();

			this.cmbProfile.DisplayMember = "Name";
			this.cmbProfile.DataSource = profiles;

			if(this.cmbProfile.Items.Count > 0)
			{
				this.cmbProfile.SelectedIndex = 0;
			}

			int i = this.cmbProfile.FindStringExact(Settings.ProfileName);

			if(i >= 0)
			{
				this.cmbProfile.SelectedIndex = i;
			}
		}

		private void InitEntrySelection(PwGroup rootGroup)
		{
			this.AddEntriesToTreeView(rootGroup);
			this.ctvEntrySelection.Refresh();
		}

		private void AddEntriesToTreeView(PwGroup group, TreeNode parentNode = null)
		{
			if(this.GroupHasEntries(group))
			{
				TreeNode groupNode = null;

				if(parentNode != null)
				{
					groupNode = parentNode.Nodes.Add(string.Format("{0} {1}", INDICATOR_GROUP, group.Name));
				}
				else
				{
					groupNode = this.ctvEntrySelection.Nodes.Add(string.Format("{0} {1}", INDICATOR_GROUP, group.Name));
				}

				foreach(PwEntry entry in group.Entries)
				{
					groupNode.Nodes.Add(string.Format("{0} {1}", INDICATOR_ENTRY, entry.Strings.ReadSafe(PwDefs.TitleField))).Tag = entry;
				}

				if(group.Groups != null)
				{
					foreach(PwGroup subGroup in group.Groups)
					{
						this.AddEntriesToTreeView(subGroup, groupNode);
					}
				}
			}
		}

		private bool GroupHasEntries(PwGroup group)
		{
			bool groupHasEntries = false;

			if(group.Entries != null && group.Entries.UCount > 0)
			{
				groupHasEntries = true;
			}
			else
			{
				if(group.Groups != null && group.Groups.UCount > 0)
				{
					IEnumerator<PwGroup> groupEnumerator = group.Groups.GetEnumerator();
					groupEnumerator.Reset();

					while(!groupHasEntries && groupEnumerator.MoveNext())
					{
						groupHasEntries = this.GroupHasEntries(groupEnumerator.Current);
					}
				}
			}

			return groupHasEntries;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			PwProfile profile = (PwProfile)cmbProfile.SelectedItem;

			if(profile == null)
			{
				Util.ShowInformationBox("Please select a profile");
				return;
			}

			List<PwEntry> selectedEntries = this.GetSelectedEntries();

			if(selectedEntries == null || selectedEntries.Count == 0)
			{
				Util.ShowInformationBox("Please select at least one entry.");
				return;
			}

			bool writeChangesToFile = this.chkOutputChangesToFile.Checked;
			string outputFilePath = this.txtOutputFile.Text;
			string outputFileFormat = this.txtFileEntryFormat.Text;

			if(writeChangesToFile && !Util.IsPathValid(outputFilePath))
			{
				Util.ShowWarningBox("The specified output path is not valid.");
				return;
			}

			this.SelectedEntries = selectedEntries;

			Settings.SetValues(writeChangesToFile, outputFilePath, outputFileFormat, profile.Name);

			this.DialogResult = DialogResult.OK;
		}

		private List<PwEntry> GetSelectedEntries()
		{
			return this.GetSelectedEntries(this.ctvEntrySelection.Nodes);
		}

		private List<PwEntry> GetSelectedEntries(TreeNodeCollection nodeCollection)
		{
			List<PwEntry> selectedEntries = new List<PwEntry>();

			foreach(TreeNode node in nodeCollection)
			{
				if(node.Tag != null && node.Tag.GetType() == typeof(PwEntry) && node.Checked)
				{
					selectedEntries.Add((PwEntry)node.Tag);
				}

				if(node.Nodes != null && node.Nodes.Count > 0)
				{
					selectedEntries.AddRange(this.GetSelectedEntries(node.Nodes));
				}
			}

			return selectedEntries;
		}

		private void btnSelectOutputFile_Click(object sender, EventArgs e)
		{
			using(SaveFileDialog sfd = new SaveFileDialog())
			{
				if(sfd.ShowDialog() == DialogResult.OK)
				{
					this.txtOutputFile.Text = sfd.FileName;
				}
			}
		}

		private void tvEntrySelection_AfterCheck(object sender, TreeViewEventArgs e)
		{
			if(!blockEventHandling)
			{
				blockEventHandling = true;

				this.CheckDown(e.Node);
				this.CheckUp(e.Node);

				blockEventHandling = false;
			}
		}

		private void CheckUp(TreeNode node)
		{
			TreeNode parentNode = node.Parent;

			if(parentNode != null)
			{
				bool parentHasCheckedChildren = false;

				IEnumerator treeNodeEnumerator = parentNode.Nodes.GetEnumerator();

				while(!parentHasCheckedChildren && treeNodeEnumerator.MoveNext())
				{
					TreeNode currentNode = (TreeNode)treeNodeEnumerator.Current;
					parentHasCheckedChildren = currentNode.Checked;
				}

				parentNode.Checked = parentHasCheckedChildren;
				this.CheckUp(parentNode);
			}
		}

		private void CheckDown(TreeNode node)
		{
			if(node.Nodes != null && node.Nodes.Count > 0)
			{
				foreach(TreeNode childNode in node.Nodes)
				{
					childNode.Checked = node.Checked;
					this.CheckDown(childNode);
				}
			}
		}

		private void chkOutputChangesToFile_CheckedChanged(object sender, EventArgs e)
		{
			this.ToggleFileOutput();
		}

		private void initPlaceholderSelection()
		{
			this.cmsPlaceholderSelection.Items.Add(MagicStrings.PLACEHOLDER_PATH + " - Der Pfad zum geänderten Eintrag").Tag = MagicStrings.PLACEHOLDER_PATH;
			this.cmsPlaceholderSelection.Items.Add(MagicStrings.PLACEHOLDER_USER + " - Der Benutzer des geänderten Eintrags").Tag = MagicStrings.PLACEHOLDER_USER;
			this.cmsPlaceholderSelection.Items.Add(MagicStrings.PLACEHOLDER_NEWPASS + " - Das neue Passwort des geänderten Eintrags").Tag = MagicStrings.PLACEHOLDER_NEWPASS;
			this.cmsPlaceholderSelection.Items.Add(MagicStrings.PLACEHOLDER_OLDPASS + " - Der alte Passwort des geänderten Eintrags").Tag = MagicStrings.PLACEHOLDER_OLDPASS;

			foreach(ToolStripMenuItem tsmi in this.cmsPlaceholderSelection.Items)
			{
				tsmi.Click += tsmi_Click;
			}
		}

		private void tsmi_Click(object sender, EventArgs e)
		{
			ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
			string placeholder = (string)tsmi.Tag;

			this.txtFileEntryFormat.Paste(placeholder);
		}

		private void btnPlaceholderSelection_Click(object sender, EventArgs e)
		{
			System.Drawing.Point p = this.btnPlaceholderSelection.PointToScreen(new System.Drawing.Point(0, this.btnPlaceholderSelection.Height));
			this.cmsPlaceholderSelection.Show(p);
		}
	}
}